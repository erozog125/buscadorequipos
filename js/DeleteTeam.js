function DeleteCardTeam() {
  const id = this.id; 
  
  const card = document.getElementById(id.replace('delete', 'card'));

  if ( card ) {
    card.remove();
  }
}  