// Variables
const nombreT = document.querySelector('#nombre');
const year = document.querySelector('#year');
const min = document.querySelector('#minimo');
const max = document.querySelector('#maximo');
const color = document.querySelector('#color');

const resultado = document.querySelector('#resultado');

const datosBusqueda = {
  nombre: '',  
  year: '',
  min: '',
  max: '',
  color: ''
}
// Eventos select para la busqueda
llenarObjetoBusqueda();
function llenarObjetoBusqueda() {
  llenarNombre();
  llenarYear();
  llenarMin();
  llenarMax();
  llenarColor();
}


function llenarNombre() {
  nombreT.addEventListener('change', (e)=> {
    datosBusqueda.nombre = e.target.value;
    filtrarEquipo();    
  });  
}
function llenarYear() {
  year.addEventListener('change', (e)=> {
    datosBusqueda.year = parseInt(e.target.value);    
    filtrarEquipo();    
  });
}
function llenarMin() {
  min.addEventListener('change', (e)=> {
    datosBusqueda.min = e.target.value;
    filtrarEquipo();
  });
}
function llenarMax() {
  max.addEventListener('change', (e)=> {
    datosBusqueda.max = e.target.value;    
    filtrarEquipo();
  });  
}
function llenarColor() {
  color.addEventListener('change', (e)=> {
    datosBusqueda.color = e.target.value;    
    filtrarEquipo();
  });  
}

// Funciones
document.addEventListener('DOMContentLoaded', ()=> {
  mostrarEquipos(equipos); //Muestra toda la info de los equipos al cargar el DOM 
  generarSelect(); // Con esta función vamos a llenar los select
});

function mostrarEquipos(equipos) {
  
  limpiarHtml();

  equipos.forEach(equipo => {
    
    const { nombre, descripcion, site, year, precio, color } = equipo;
    const miEquipo = document.createElement('p');    
    miEquipo.textContent = `Nombre: ${nombre} - Descripción: ${descripcion} - Año de Creación: ${year} - Precio: ${precio} - Color: ${color} - Sitio Oficial: `;  
    const link = document.createElement('a');
    link.textContent = site;
    link.setAttribute('href', site);
    link.setAttribute('target', '_blank');
    miEquipo.appendChild(link);
    resultado.appendChild(miEquipo);

  });

}

function generarSelect() {
  generarNombres();  
  generarYear();  
  generarMin();
  generarMax();    
}

function generarMin_Max() {
  let min_max_Equipos = [];
  equipos.map( equipo => {
    if (!(min_max_Equipos.includes(equipo.precio))) {
      min_max_Equipos.push(equipo.precio);      
    } 
  })  
  min_max_Equipos = min_max_Equipos.sort((a,b) => a - b);
  return min_max_Equipos  
}

function generarMax() {
  const maxEquipos = generarMin_Max();
  maxEquipos.forEach( maxElement => {
    const opcion = document.createElement('option');
    opcion.value = maxElement;
    opcion.textContent = maxElement;
    max.appendChild(opcion);
  } )
}

function generarMin() {
  const minEquipos = generarMin_Max();
  minEquipos.forEach( minElement => {
    const opcion = document.createElement('option');
    opcion.value = minElement;
    opcion.textContent = minElement;
    min.appendChild(opcion);
  } )}

function generarYear() {
  // Llenar el select con los años de cada equipo
  // Voy  a crear un nuevo arreglo solo con los años para ordenarlos
  let yearTeams = [];
  yearsTeams = equipos.map(equipo => yearTeams.push(equipo.year));  
  yearsTeam = yearTeams.sort();

  yearTeams.forEach(objYear => {
    const opcion = document.createElement('option');
    opcion.value = objYear;
    opcion.textContent = objYear;
    year.appendChild(opcion);    
  })
}

function generarNombres() {  
  let nombreEquipos = [];
  equipos.map( equipo => nombreEquipos.push(equipo.nombre) );  
  nombreEquipos.sort();  
  nombreEquipos.forEach( obEquipo => {
    const opcion = document.createElement('option');
    opcion.value = obEquipo;
    opcion.textContent = obEquipo;
    nombreT.appendChild(opcion);
  });
}

function filtrarEquipo() {
  const resultado = equipos.filter(filtrarNombre).filter(filtrarYear).filter(filtrarMinimo).filter(filtrarMaximo).filter(filtrarColor);
  console.log(resultado);
  
  if (resultado.length) {
    mostrarEquipos(resultado);
  } else {
    noResultado();
  }
}

function noResultado() {

  limpiarHtml();
  
  const noResultado = document.createElement('div');
  noResultado.classList.add('alerta','error');
  noResultado.textContent = 'Tu búsqueda no arroja ningún resultado';
  resultado.appendChild(noResultado);
}

function filtrarNombre(equipo) {
  if ( datosBusqueda.nombre ) {
    return equipo.nombre === datosBusqueda.nombre
  }
  else return equipo;
}

function filtrarYear(equipo) {
  const { year } = datosBusqueda;
  if ( year ) {
    return equipo.year === year;
  }
  else return equipo;
}

function filtrarColor(equipo) {
  const { color } = datosBusqueda;
  if ( color ) {
    return equipo.color === color;
  }
  else return equipo;
}

function filtrarMinimo(equipo) {
  const { min } = datosBusqueda;
  if ( min ) {
    return equipo.precio >= min;
  }
  else return equipo;
}

function filtrarMaximo(equipo) {
  const { max } = datosBusqueda;
  if ( max ) {
    return equipo.precio <= max;
  }
  else return equipo;
}

//limpiar HTML
function limpiarHtml() {
  resultado.innerHTML = '';
}