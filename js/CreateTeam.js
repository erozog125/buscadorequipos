const createTeam = document.querySelector('#menuCreate');
const btnClear = document.querySelector('#btnClear');
const selectTeams = document.querySelector('#selectTeams');
const btnCreate = document.querySelector('#btnCrear');
const mySelect = document.querySelector('#buscadorCrud');
const btnDelete = document.querySelector('#btnDelete');
const myCards = document.querySelector('.myCards');
const myBody = document.querySelector('body');

createTeam.addEventListener('click',menuCreate);
btnCreate.addEventListener('click', createCardTeam);
btnClear.addEventListener('click', clearCards);
let nombreEquipos = [];

function fillSelect() {
 
  const element = document.getElementById("selectTeams");
  while (element.firstChild) {
    element.removeChild(element.firstChild);
  }
  equipos.map( equipo => {
    const option = document.createElement('option');
    option.value = equipo.nombre;
    option.textContent = equipo.nombre;
    selectTeams.appendChild(option);
  });

}

function menuCreate() {
  fillSelect();
  update.style.visibility = 'hidden';
  mySelect.style.display = 'flex';
  btnClear.style.display = 'flex';
  btnCreate.style.display = 'flex';         
};

function createCardTeam() {
 
  const selectItem = selectTeams.value;
  const team = equipos.find( equipo => equipo.nombre === selectItem);

  if ( !document.getElementById(`card-${team.id}`) ) {    
    createElement(team);
  }
}

function createElement(equipo) {
      const divCol = document.createElement('div');
      divCol.classList.add('col');                                                                                                                                                       
      divCol.setAttribute('id', `card-${equipo.id}`);
      const divCard = document.createElement('div');        
      divCard.classList.add('card');
      const btnDel = document.createElement('button');
      btnDel.classList.add('btnDel');
      btnDel.setAttribute('id', `delete-${equipo.id}`);
      btnDel.textContent = 'X';        
      btnDel.addEventListener('click', DeleteCardTeam);        
      divCard.appendChild(btnDel);                
      const imgTeam = document.createElement('img');
      imgTeam.classList.add('card-img-top');
      imgTeam.setAttribute('src', equipo.img);      
      imgTeam.setAttribute('alt', equipo.nombre);
      const divCardBody = document.createElement('div');
      divCardBody.classList.add('card-body');
      const h5Tittle = document.createElement('h5');
      h5Tittle.classList.add('card-title');
      h5Tittle.textContent = equipo.nombre;
      const pDescription = document.createElement('p');
      pDescription.classList.add('card-text');
      pDescription.textContent = equipo.descripcion;      
      const alink = document.createElement('a');
      alink.setAttribute('target','_blank');
      alink.setAttribute('href',equipo.site);
      alink.classList.add('btn');
      alink.classList.add('btn-primary');
      alink.textContent = 'See more...';        
      divCol.appendChild(divCard);        
      divCard.appendChild(imgTeam);
      divCard.appendChild(divCardBody);
      divCardBody.appendChild(h5Tittle);
      divCardBody.appendChild(pDescription);
      divCardBody.appendChild(alink);
      myCards.appendChild(divCol);    
} 

function clearCards() {  
  myCards.innerHTML = '';
}    