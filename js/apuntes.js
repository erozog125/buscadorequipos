const form = document.querySelector('#form');
const listNotes = document.querySelector('#saveNotes');
let myNotes = [];

eventListeners();
function eventListeners() {
  form.addEventListener('submit', addNote);

  document.addEventListener('DOMContentLoaded', () => {
    myNotes = JSON.parse( localStorage.getItem('notes')) || [];
      showNotes();
  });
}

function addNote(e) {
  e.preventDefault();
  const notes = document.querySelector('#notes').value;
  
  if (notes === '') {
    showError('El apunte no puede ir vacío');
    return null;
  }   
  myNotes = [...myNotes,notes];
  showNotes();
  form.reset();
}

function showError(error) {
  const msjError = document.createElement('p');
  msjError.textContent = error;
  msjError.classList.add('error');
  listNotes.appendChild(msjError);

  setTimeout( ()=> {
    msjError.remove()
  },3000);

}

function showNotes() {

  cleanNotes();

  if ( myNotes.length > 0 ) {

    myNotes.forEach ( notes => {
      const btnDelete = document.createElement('a');
      btnDelete.textContent = 'X';
      btnDelete.classList.add('btnDelete');      
      btnDelete.setAttribute('href','#');

      const li = document.createElement('li');
      li.textContent = notes;
      li.setAttribute('id',`${notes}`);  
      li.appendChild(btnDelete);
      listNotes.appendChild(li);
      
      btnDelete.onclick = () => { 
        cleanNote(li.getAttribute('id'));
       };
    });
  }

  loadLocalstorage();
}

function cleanNotes() {
  while (listNotes.firstChild) {
    listNotes.removeChild(listNotes.firstChild);
  }
}
function loadLocalstorage() {
  localStorage.setItem('notes',JSON.stringify(myNotes));
}

function cleanNote(id) {
  myNotes = myNotes.filter(note => note !== id);
  showNotes();
}