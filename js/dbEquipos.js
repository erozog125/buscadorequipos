const equipos = [
  {
    id: 1,
    nombre: 'Millonarios Fc',
    descripcion: `El Azul y Blanco Millonarios Fútbol Club S. A.​, más conocido como Millonarios, ​ es un club de fútbol de la ciudad de Bogotá, capital de Colombia.Wikipedia.` ,
    img: '../assets/img/teams/millos.png',
    site: 'https://millonarios.com.co/',
    year: 1946,
    precio: 1000,    
    color: 'AZUL'
  },
  {
    id: 2,
    nombre: 'America de Cali',
    descripcion: `El América de Cali S. A. —en forma abreviada: América de Cali o América— es un club de fútbol colombiano fundado el 13 de febrero de 1927 en la ciudad de Cali. Es considerado uno de los clubes más grandes y populares de Colombia y uno de los más importantes en Sudamérica.​ Wikipedia`,
    img: '../assets/img/teams/america.jpg',
    site: 'https://www.americadecali.co/',
    year: 1927,
    precio: 1100,
    color: 'ROJO'  
  },
  {
    id: 3,
    nombre: 'Atlético Nacional',
    descripcion: `Atlético Nacional es un club de fútbol de la ciudad de Medellín, capital del departamento de Antioquia, Colombia. Es considerado uno de los clubes más populares de Colombia y de Sudamérica.​ Wikipedia`,
    img: '../assets/img/teams/nacional.jpg',
    site: 'https://www.atlnacional.com.co/',
    year: 1947,
    precio: 2400,    
    color: 'VERDE'  
  },
  {
    id: 4,
    nombre: 'Independiente Santafé',
    descripcion: `El Club Independiente Santa Fe​ ​ ​ mejor conocido como Independiente Santa Fe o simplemente Santa Fe, es un club de fútbol fundado el 28 de febrero de 1941, en el centro histórico de la ciudad de Bogotá, capital de Colombia. Wikipedia`,
    img: '../assets/img/teams/santafe.jpg',
    site: 'https://independientesantafe.com/',
    year: 1941,
    precio: 1100,    
    color: 'ROJO'        
  },
  {
    id: 5,
    nombre: 'Deportivo Cali',
    descripcion: `El Deportivo Cali​ es un club deportivo de la ciudad de Cali, en el Valle del Cauca, Colombia, fundado el 23 de noviembre de 1912 como que después de pasar por dos re-estructuraciones desde su fundación.Wikipedia`,
    img: '../assets/img/teams/cali.png',
    site: 'https://deportivocali.com.co/',
    year: 1912,
    precio: 1300,    
    color: 'VERDE'        
  },
  {
    id: 6,
    nombre: 'Independiente Medellín',
    descripcion: `El Deportivo Independiente Medellín, más conocido como Independiente Medellín, Medellín, o simplemente por sus tres siglas el DIM, es un club de fútbol de Colombia fundado bajo el nombre de «Medellín Football Club» el 14 de noviembre de 1913 por Alberto Uribe Piedrahíta. Wikipedia`,
    img: '../assets/img/teams/dim.jpg',
    site: 'https://dimoficial.com/',
    year: 1913,
    precio: 850,    
    color: 'ROJO'        
  },  
  {
    id: 7,
    nombre: 'Águilas Doradad Rionegro',
    descripcion: `Águilas Doradas Rionegro​ es un club de fútbol colombiano que tiene base en el municipio de Rionegro al oriente del departamento de Antioquia. Fue fundado el 16 de julio de 2008 y actualmente Juega en la Categoría Primera A luego de obtener el título de la Primera B en 2010. Wikipedia`,
    img: '../assets/img/teams/aguilas.jpeg',
    site: 'https://aguilasdoradas.com.co/',
    year: 2008,
    precio: 500,    
    color: 'DORADO'        
  },  
  {
    id: 8,
    nombre: 'Corporación deportiva Alianza Petrolera',
    descripcion: `El Alianza Petrolera es un club de fútbol de Colombia, originario del municipio de Barrancabermeja, en el departamento de Santander, fundado el 24 de octubre de 1991. El club debutó profesionalmente en la Categoría Primera B en la temporada 1992.​ Wikipedia`,
    img: '../assets/img/teams/alianza.jpeg',
    site: 'https://www.alianzapetrolerafc.com/welcome.html',
    year: 1991,
    precio: 500,    
    color: 'AMARILLO'        
  },  
  {
    id: 9,
    nombre: 'Club Atlético Bucaramanga',
    descripcion: `El Club Atlético Bucaramanga​ es un club de fútbol de Colombia ubicado en Bucaramanga, capital del departamento de Santander, equipo de la Categoría Primera A, máxima categoría de la División Mayor del Fútbol Colombiano. Wikipedia`,
    img: '../assets/img/teams/bucaramanga.jpeg',
    site: 'https://atleticobucaramanga.com.co/',
    year: 1948,
    precio: 500,    
    color: 'AMARILLO'        
  },  
  { 
    id: 10,
    nombre: 'Once Caldas S.A.',
    descripcion: `El Once Caldas S.A., o mayormente conocido como Once Caldas, ​ es un club de fútbol de Colombia. Tiene su sede en la ciudad de Manizales, departamento de Caldas. Wikipedia`,
    img: '../assets/img/teams/caldas.png',
    site: 'https://www.oncecaldas.com.co/',
    year: 1961,
    precio: 900,    
    color: 'BLANCO'        
  },  
  { 
    id: 11,
    nombre: 'Boyacá Chicó F.C.',
    descripcion: `El Boyacá Chicó Fútbol Club es un club de fútbol colombiano que juega en la ciudad Tunja en el departamento de Boyacá. Fue fundado el 26 de marzo de 2002, y es la primera sociedad anónima deportiva en Colombia.​​​ Wikipedia`,
    img: '../assets/img/teams/chico.png',
    site: 'https://www.transfermarkt.es/boyaca-chico-fc/startseite/verein/14649',
    year: 2002,
    precio: 400,    
    color: 'BLANCO'        
  },  
  { 
    id: 12,
    nombre: 'Envigado F.C.',
    descripcion: `El Envigado Fútbol Club, es un club de fútbol Colombia, del municipio de Envigado, en el sur del departamento de Antioquia. Fue fundado en 1989 y actualmente juega en la Categoría Primera A de Colombia. Wikipedia`,
    img: '../assets/img/teams/envigado.png',
    site: 'https://www.transfermarkt.es/boyaca-chico-fc/startseite/verein/14649',
    year: 1989,
    precio: 350,    
    color: 'NARANJA'        
  },  
  { 
    id: 13,
    nombre: 'Club deportivo La Equidad Seguros',
    descripcion: `El Club Deportivo La Equidad, cuya razón social es Club Deportivo La Equidad Seguros Sociedad Anónima, ​ también conocido simplemente como La Equidad, es un club de fútbol de la ciudad de Bogotá, Colombia. Wikipedia`,
    img: '../assets/img/teams/equidad.jpeg',
    site: 'https://equidadclubdeportivo.coop/',
    year: 1982,
    precio: 750,    
    color: 'VERDE'        
  },  
  { 
    id: 14,
    nombre: 'Jaguares FC S.A.',
    descripcion: `El Jaguares de Córdoba Fútbol Club S. A., comúnmente conocido como Jaguares de Córdoba, o simplemente Jaguares, es un club de fútbol colombiano pertenece a la ciudad de Montería, Córdoba Colombia. Fue fundado el 5 de diciembre de 2012, y actualmente juega en la Categoría Primera A del Fútbol Profesional Colombiano.​ Wikipedia`,
    img: '../assets/img/teams/jaguares.png',
    site: 'https://jaguaresfc.com.co/',
    year: 2012,
    precio: 350,    
    color: 'AZUL'        
  },  
  { 
    id: 15,
    nombre: 'Deportes Quindío',
    descripcion: `La Corporación Deportes Quindío es un club de fútbol colombiano de la ciudad de Armenia, capital del departamento de Quindío. Fue fundado el 8 de enero de 1951​​ y actualmente juega en la Categoría Primera B de Colombia. Wikipedia`,
    img: '../assets/img/teams/quindio.jpeg',
    site: 'http://www.deportesquindio.com.co/',
    year: 1951,
    precio: 350,    
    color: 'VERDE'        
  },  
  { 
    id: 16,
    nombre: 'Junior de Barranquilla',
    descripcion: `El Club Deportivo Popular Junior, conocido como Junior de Barranquilla, o simplemente Junior, e igualmente por su antiguo nombre, Atlético Junior, es un equipo de fútbol con sede en Barranquilla, Colombia. Fue fundado el 7 de agosto de 1924, convirtiéndose en el tercer club más antiguo del país.​ Wikipedia`,
    img: '../assets/img/teams/junior.jpeg',
    site: 'http://app.juniorfc.co/',
    year: 1924,
    precio: 1100,    
    color: 'ROJO'        
  },  
  { 
    id: 17,
    nombre: 'Asociación Deportivo Pasto',
    descripcion: `La Asociación Deportivo Pasto, también conocido como Deportivo Pasto, o, simplemente, Pasto; es un club de fútbol colombiano de la ciudad de San Juan de Pasto, en el departamento de Nariño. Fue fundado el 12 de octubre de 1949.​ Wikipedia`,
    img: '../assets/img/teams/pasto.jpeg',
    site: 'https://deportivopasto.com.co/',
    year: 1949,
    precio: 650,    
    color: 'ROJO'        
  },  
  { 
    id: 18,
    nombre: 'Patriotas FC',
    descripcion: `El Patriotas Boyacá o simplemente Patriotas es un club de fútbol colombiano del departamento de Boyacá, que juega en la Primera División de fútbol profesional colombiano. Wikipedia`,
    img: '../assets/img/teams/patriotas.jpeg',
    site: 'https://www.patriotas.co/',
    year: 2003,
    precio: 400,    
    color: 'ROJO'        
  },  
  { 
    id: 19,
    nombre: 'Deportivo Pereira',
    descripcion: `El Deportivo Pereira, llamado oficialmente por su razón social Corporación Social Deportiva y Cultural de Pereira Corpereira, ​ es un club de fútbol colombiano de la ciudad de Pereira, capital del departamento de Risaralda, y fue fundado el 12 de febrero de 1944. Wikipedia`,
    img: '../assets/img/teams/pereira.png',
    site: 'https://www.deportivopereira.com.co/',
    year: 1944,
    precio: 520,    
    color: 'AMARILLO'        
  },  
  { 
    id: 20,
    nombre: 'Deportes Tolima',
    descripcion: `El Club Deportes Tolima S.A, más conocido como Deportes Tolima, es un club de fútbol con sede en Ibagué Colombia. Fue fundado el 18 de diciembre de 1954 y compite en la Categoría Primera A del fútbol colombiano. Wikipedia`,
    img: '../assets/img/teams/tolima.jpeg',
    site: 'https://clubdeportestolima.com.co/',
    year: 1944,
    precio: 1000,    
    color: 'AMARILLO'        
  },  
];