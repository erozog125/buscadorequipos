const updateTeam = document.querySelector('#menuUpdate');
const btnUpdate = document.querySelector('#btnUpdate');
const btnCancel = document.querySelector('#btnCancel');
const update = document.querySelector('.updateForm');
const title = document.querySelector('#SelectedEquipo'); 

selectTeams.addEventListener('change', updateSelect);

function updateSelect () {
  title.textContent = selectTeams.value;
}

updateTeam.addEventListener('click', createFormUpdate);
btnUpdate.addEventListener('click', updateCard);

function createFormUpdate() {
  clearCards();
  menuCreate();
  update.style.visibility = 'visible';
  btnClear.style.display = 'none';
  btnCreate.style.display = 'none';    
}

function updateCard (e) {
  e.preventDefault();  
  const msj = confirm('Sure?');
  if( msj ) {
  const name = document.querySelector('#nameForm');
  const description = document.querySelector('#descriptionForm');  
  const team = equipos.find( equipo => equipo.nombre === selectTeams.value);  
  team.nombre = name.value;
  team.descripcion = description.value;
  alert('Done!');
  }  
}

